# Azur Gears

Contains description and stats for Azur Lane equipment. You can filter and sort weapons by their stats, compare any ship, view shell modifiers, blueprints droplist, etc.

Link: https://azurgears.netlify.app/