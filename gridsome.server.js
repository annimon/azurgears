const _ = require('lodash')
const axios = require('axios')
const jsYaml = require('js-yaml')
const fs = require('fs')
const graphql = require('graphql')

const {
  GraphQLObjectType,
  GraphQLInt,
} = graphql

module.exports = function (api) {
  api.loadSource(async store => {
    const sectionsType = store.addCollection('Section')
    const data = require('./src/data/data.js')
    for (const section of data.sections) {
      sectionsType.addNode({
        id: section.id,
        title: section.name,
        pos: parseInt(section.pos),
        section: section.section === true,
        icon: section.icon,
        bg: section.bg
      })
    }

    const gearsType = store.addCollection('Gears')
    let sectionIndex = 0
    for (const key in data.gears) {
      sectionIndex++

      const typeName = key[0].toUpperCase() + key.replace(/-([a-z])/gi, g => g[1].toUpperCase()).slice(1)
      const gearType = store.addCollection('Gear' + typeName)

      for (const gear of data.gears[key]) {
        gearType.addNode(_.assign(gear, {
          id: gear.name,
          title: gear.name
        }))

        // Add to global gears
        if (!gear.isDuplicate) {
          gearsType.addNode(_.assign(gear, {
            id: key + '-' + gear.name,
            title: gear.name,
            pos: (sectionIndex * 100 + gear.pos),
            category: key
          }))
        }
      }
    }

    const ships = jsYaml.safeLoad(fs.readFileSync('./src/data/ships.yml', 'utf8'))
    const shipsType = store.addCollection('Ships')
    const shipLevelStatsType = new GraphQLObjectType({
      name: 'ShipLevelStats',
      fields: {
        health: {type: GraphQLInt},
        firepower: {type: GraphQLInt},
        antiAir: {type: GraphQLInt},
        torpedo: {type: GraphQLInt},
        evasion: {type: GraphQLInt},
        aviation: {type: GraphQLInt},
        cost: {type: GraphQLInt},
        reload: {type: GraphQLInt},
        speed: {type: GraphQLInt},
        asw: {type: GraphQLInt},
        oxygen: {type: GraphQLInt},
        ammo: {type: GraphQLInt},
        accuracy: {type: GraphQLInt},
        luck: {type: GraphQLInt}
      }
    })

    for (const ship of ships) {
      if (ship.id.toString().startsWith('Collab')) {
        ship.id = ship.id.replace('Collab', 'C')
        ship.globalType = 'Collab'
      } else if (ship.id.toString().startsWith('Plan')) {
        ship.id = ship.id.replace('Plan', 'P')
        ship.globalType = 'Plan'
      } else if ((parseInt(ship.id) || 0) > 3000) {
        ship.globalType = 'Retrofit'
      } else {
        ship.globalType = 'Normal'
      }
      shipsType.addNode(ship)
    }

    const commitsType = store.addCollection('Commits')
    const commits = await axios.get('https://gitlab.com/api/v4/projects/annimon%2Fazurgears/repository/commits')
    for (const commit of commits.data) {
      commitsType.addNode({
        id: commit.id,
        title: commit.title,
        date: commit.created_at
      })
    }

  })
}