import '~/assets/styles/spectre.min.css'
import '~/assets/styles/spectre-exp.min.css'
import '~/assets/styles/spectre-icons.min.css'
import '~/assets/styles/style.css'

import DefaultLayout from '~/layouts/Default.vue'
import DropInfo from '~/components/DropInfo.vue'
import Spoiler from '~/components/Spoiler.vue'

export default function (Vue, { router, head, isClient }) {
  head.htmlAttrs = { lang: 'ru' }

  Vue.directive('click-outside', {
    bind: function (el, binding, vnode) {
      el.clickOutsideEvent = function (event) {
        if (!(el == event.target || el.contains(event.target))) {
          vnode.context[binding.expression](event)
        }
      }
      document.body.addEventListener('click', el.clickOutsideEvent)
    },
    unbind: function (el) {
      document.body.removeEventListener('click', el.clickOutsideEvent)
    }
  })
  Vue.component('Layout', DefaultLayout)
  Vue.component('DropInfo', DropInfo)
  Vue.component('Spoiler', Spoiler)
}
